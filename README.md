#################### Creating Staging Amazon Customer Order Table ######################################

Create table stg_amz_cust_ord(	
	amz_acct varchar(50),
	ord_id varchar (100), 
	ord_dt varchar (50), 
	ord_stus varchar (100),
	flmt_chnl varchar (100),
	sls_chnl varchar (100), 
	prdct_nm varchar (2000), 
	sku varchar (500),  
	asin varchar (10) not null distkey,
	qt_ordd integer,
	ccy varchar (10),
	ttl_itm_px decimal (10,2),
	ttl_itm_tx decimal (10,2),
	shp_st varchar (100));

#################### Creating Staging Inventory Age Table ######################################

Create table stg_inv_age(
	amz_acct varchar(50), 	
	inv_dt varchar (50), 
	sku varchar (500),
	fnsku varchar (10), 
	asin varchar(10) not null distkey, 
	prdct_nm varchar (2000),
	qt_avlb integer, 
	qt_0_90 integer,
	qt_91_180 integer, 
	qt_181_270 integer, 
	qt_271_365 integer,
	qt_365_plus integer, 
	sls_past_60 integer);


#################### Creating Staging FBA Inventory Table ######################################

Create table stg_fba_inv(	
	amz_acct varchar(50), 
	fba_inv_dt varchar (50),
        sku varchar (500),
	fnsku varchar(50),
	asin varchar(10) not null distkey,  
	prdct_nm varchar (2000),
        sls_px decimal (10,2), 
        flbl_qt integer); 
	


#################### Creating Staging Teika Export Table ######################################
Create table stg_teika_expt (	
	asin varchar (10) not null distkey,
	prdct_cst decimal (10,2),
	splr varchar (500));


#################### Creating Staging Teika Orders Table ######################################

Create table stg_prch_ords(	
	splr varchar (500) not null distkey,
        po_amt decimal (10,2),
	in_tran_amt decimal (10,2), 
	crte_dt varchar(50));


#################### Creating Staging Accounts Table ######################################
 

Create table stg_accts(	
	acct_id varchar (100) not null distkey,
	splr_nm varchar (500),
        acct_stus varchar (100),
	acct_mgr varchar(100), 
	inv_mgr varchar (100),
        free_shpg varchar (100), 
        drct_shpg varchar (10),
 	cc_acptd varchar (10),
	quad varchar (10), 
	zip varchar(100), 
	ctgy varchar (100), 
	amz_acct varchar (100),
	amify_offr_ty varchar (100));




#################### Creating Staging Fee Table ######################################

Create table stg_prdct_fee(
	amz_acct varchar(50), 
	asin varchar (10), 
	prdct_wght float,
	wght_typ varchar(10), 
	ccy varchar (10),
	prdct_fee decimal(10,2), 
        lgst_sd     numeric(10,2),
        mdn_sd      numeric(10,2),
        shrt_sd     numeric(10,2),
	est_ref_fee numeric(10,2)); 


#################### Creating Staging Case Status Table ######################################

create table stg_case_status(
	transaction_item_id bigint, 
	fnsku varchar(15),
	status varchar (50),
	case_id bigint, 
	refund_amount decimal (10,2),
	recovered_qty integer, 
	submitted_date varchar(50),
	resolved_date varchar(50));


#################### Creating Staging Adj Inventory Table ######################################

CREATE TABLE stg_adj_inv
(
   seller               varchar(10),
   reportdate           varchar(500),
   adjusteddate         varchar(50),
   transactionitemid    varchar(100),
   fnsku                varchar(20),
   sku                  varchar(500),
   productname          varchar(2000),
   fulfillmentcenterid  varchar(5),
   quantity             integer,
   reason               varchar(5),
   disposition          varchar(50)
);



#################### Creating staging FBA Returns table  ######################################

CREATE TABLE stg_fba_returns
(
amz_account           varchar(10), 
return_date           varchar(500),
order_id              varchar(100), 
sku                   varchar(500), 
asin	              varchar(10), 
fnsku                 varchar(10), 
product_name          varchar(500), 
quantity              integer,
fulfillment_center_id varchar(10), 
detailed_disposition  varchar(500),
reason                varchar(500), 
status		      varchar(2000),
license_plate_number  varchar(14), 
customer_comments     varchar(2000)
); 

#################### Creating staging Amazon Listings table  ######################################
CREATE TABLE stg_amazon_listings
(
   amz_account  varchar(10),
   listing_id   varchar(50),
   sku          varchar(500),
   open_date    varchar(500),
   asin         varchar(15),
   status       varchar(50)
);


#################### Creating Staging Teika prch_ords Table ######################################

CREATE TABLE stg_teika_purchase_order(
	po_id varchar(10),
	po_created_dt varchar (100),
	po_update_dt varchar (100),
	po_name varchar (500),
	status varchar (100),
	total_ordered integer,
	total_received integer,
	total_cost decimal (10,2),
	supplier_name varchar (100)); 




#################### Creating Staging Teika purchase order item Table ######################################


CREATE TABLE stg_teika_purchase_order_item( 
	po_id varchar(10),
	product_name varchar(500),
	sku varchar (500),
	asin varchar (15), 
	mpn varchar (500), 
	total_ordered integer,
	total_received integer,
	vendor_cost decimal (10,2),
	supplier_name	varchar(100), 
	po_create_dt varchar (100),
	po_update_dt varchar(100)); 



#################### Creating Staging Teika product Table ######################################


CREATE TABLE stg_teika_product( 
asin varchar(10), 
sku varchar(500), 
floor decimal (10,2),
ceiling decimal (10,2),
price_match_percent decimal (10,2), 
price_match_dollar decimal (10,2), 
in_stock varchar(10), 
fba varchar(10), 
static_supplier_cost decimal (10,2),
average_supplier_cost decimal (10,2),
additional_cost decimal (10,2), 
map decimal (10,2),
prev_price decimal (10,2), 
new_price decimal (10,2), 
shipping decimal (10,2), 
total decimal (10,2), 
target_price decimal (10,2), 
change_amt decimal (10,2), 
change_pct decimal (10,2), 
product_update_dt varchar(100),
product_name varchar(500), 
supplier_name varchar(100), 
mpn varchar(500),  
repricing_active varchar(10), 
labels varchar (500), 
profile varchar (100)); 



##################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################



#################### Creating REAL Inventory Age Table ######################################

Create table inv_age(	
	amz_acct varchar(50),
        inv_dt date,  
	asin varchar(10) not null distkey, 
	qt_avlb integer, 
	qt_0_90 integer,
	qt_91_180 integer, 
	qt_181_270 integer,
	qt_271_365 integer,
	qt_365_plus integer,
	sls_past_60 integer,
	crte_dttm timestamp default sysdate);


#################### Creating REAL Inventory Age Report Table ######################################

Create table inv_age_rpt(	
	amz_acct varchar(50),
        inv_dt date,
	asin varchar(10) not null distkey, 
	qt_avlb integer, 
	qt_0_90 integer,
	qt_91_180 integer, 
	qt_181_270 integer,
	qt_271_365 integer,
	qt_365_plus integer,
	sls_past_60 integer,
	crte_dttm timestamp default sysdate);




#################### Creating REAL Amazon Customer Order Table ######################################

Create table amz_cust_ord(
	amz_acct varchar (50),	
	ord_id varchar (100), 
	ord_dt timestamp, 
	ord_stus varchar (100),
	flmt_chnl varchar (100),
	sls_chnl varchar (100),
	asin varchar (10) not null distkey, 
	qt_ordd integer, 
	ccy varchar (10), 
	ttl_itm_px decimal (10,2),
	ttl_itm_tx decimal (10,2),
	shp_st varchar (100),
	crte_dttm timestamp default sysdate,
	updt_dttm timestamp default sysdate); 



#################### Creating REAL FBA Inventory Table ######################################

Create table fba_inv(
	amz_acct varchar (50),        
	fba_inv_dt date, 	
	asin varchar(10) not null distkey,
        flbl_qt integer,
	crte_dttm timestamp default sysdate);
        

#################### Creating REAL ASIN to SKU Table ######################################

Create table asin_sku(
	asin varchar (10) not null distkey,   
	SKU varchar(500),
	crte_dttm timestamp default sysdate));



#################### Creating Product Table  ######################################
Create table weekly_prdct(
	sku varchar (500), 
	asin varchar (10) not null distkey,
	prdct_nm varchar (2000), 
	prdct_wght float,
	wght_typ varchar (10), 
	prdct_fee decimal (10,2),
	prdct_mpn varchar (500),
	prdct_cst decimal (10,2),
	prdct_stus varchar (100),
        crte_dttm timestamp default sysdate,
	updt_dttm timestamp default sysdate); 



#################### Creating REAL Product Supplier Table ###################################### Mapping Table 

Create table prdct_splr(	
	asin varchar (10) not null distkey,
	splr_nm varchar (500),
	prdct_stus varchar (100),
        crte_dttm timestamp default sysdate,
	updt_dttm timestamp default sysdate); 



#################### Creating REAL Product costs Table ###################################### 

Create table prdct_cst(	
	asin varchar (10) not null distkey,
	prdct_cst decimal (10,2),
	crte_dttm timestamp default sysdate,
	updt_dttm timestamp default sysdate);


#################### Creating REAL Teika Orders Table ######################################

Create table prch_ords(	
	splr varchar (500) not null distkey,
        po_amt decimal (10,2),
	in_tran_amt decimal (10,2), 
	crte_dt date,
	crte_dttm timestamp default sysdate);



#################### Creating REAL Accounts Table ######################################


Create table accts(	
	acct_id varchar (100) not null distkey,
	splr_nm varchar (500),
        acct_stus varchar (100),
	acct_mgr varchar(100), 
	inv_mgr varchar (100),
        free_shpg varchar (100),
        drct_shpg varchar (10),
 	cc_acptd varchar (10),
	quad varchar (10), 
	zip varchar(100), 
	ctgy varchar (100),
	amz_acct varchar (100),
	amify_offr_ty varchar (100), 
	crte_dttm timestamp default sysdate);




#################### Creating Shipping Prices Table ######################################

Create table shpg_px(	
	first_3_zip varchar (10) not null distkey,
        zone integer,
	cst_pound decimal (10,2)); 



#################### Creating REAL Fee Table ######################################

Create table prdct_fee(
	amz_acct varchar(50), 
	asin varchar (10), 
	prdct_wght float,
	wght_typ varchar(10), 
	ccy varchar (10),
	prdct_fee decimal (10,2),
	lgst_sd numeric(10,2),
        mdn_sd numeric(10,2),
        shrt_sd numeric(10,2),
	est_ref_fee numeric(10,2),
	crte_dttm timestamp default sysdate,
	updt_dttm timestamp default sysdate);


#################### Creating Real Teika prch_ords Table ######################################

CREATE TABLE teika_purchase_order(
	amz_account varchar(10), 
	po_id varchar(10),
	po_created_dt date,
	po_update_dt date,
	po_name varchar (500),
	status varchar (100),
	total_ordered integer,
	total_received integer,
	total_cost decimal (10,2),
	supplier_name varchar (100),
	create_dt timestamp default sysdate,
	update_dt timestamp default sysdate); 


#################### Creating REAL Teika purchase order item Table ######################################


CREATE TABLE teika_purchase_order_item( 
	amz_account varchar(10),
	po_id varchar(10),
	product_name varchar(500),
	sku varchar (500),
	asin varchar (15),
	mpn varchar (500), 
	total_ordered integer,
	total_received integer,
	vendor_cost decimal (10,2),
	supplier_name varchar(100), 
	po_create_dt date,
	po_update_dt date, 
	create_dt timestamp default sysdate,
	update_dt timestamp default sysdate); 



#################### Creating REAL Teika Product Export Table ######################################

CREATE TABLE teika_product( 
amz_account varchar(10), 
asin varchar(15), 
sku varchar(500), 
static_supplier_cost decimal(10,2), 
additional_cost decimal(10,2), 
product_update_dt date, 
product_name varchar(500), 
supplier_name varchar(100), 
product_status varchar(100), 
mpn varchar (500),
delete_flag boolean NOT NULL default FALSE,
create_dt timestamp default sysdate,
update_dt timestamp default sysdate);


#################### Creating REAL Case Status Table ######################################

create table case_status(
	transaction_item_id bigint, 
	fnsku varchar(15),
	status varchar (50),
	case_id bigint, 
	refund_amount decimal (10,2),
	recovered_qty integer, 
	submitted_date date,
	resolved_date date,
	create_date timestamp default sysdate,
	update_date timestamp default sysdate); 


#################### Creating REAL Adjusted Inventory Table ######################################

CREATE TABLE adj_inv(

seller               varchar(10),

reportdate           date,

adjusteddate         date,
   
transactionitemid    varchar(100),
   
fnsku                varchar(20),
   
sku                  varchar(500),
   
productname          varchar(2000),
   
fulfillmentcenterid  varchar(5),
   
quantity             integer,
   
reason               varchar(5),
   
disposition          varchar(50),
   
crte_dttm            timestamp default sysdate
);



#################### Creating REAL FBA Returns table  ######################################

CREATE TABLE fba_returns
(
amz_account           varchar(10), 
return_date           date,
order_id              varchar(100), 
sku                   varchar(500), 
asin	              varchar(10), 
fnsku                 varchar(10), 
product_name          varchar(500), 
quantity              integer,
fulfillment_center_id varchar(10), 
detailed_disposition  varchar(500),
reason                varchar(500), 
status		      varchar(500),
license_plate_number  varchar(14), 
customer_comments     varchar(2000),
create_dt	      timestamp default sysdate,
update_dt	      timestamp default sysdate
); 



#################### Creating REAL Amazon Listings table  ######################################

CREATE TABLE amazon_listings
(
   amz_account  varchar(10),
   listing_id   varchar(50),
   sku          varchar(500),
   open_date    date,
   asin         varchar(15),
   status       varchar(50),
   create_dt    timestamp      DEFAULT ('now'::text)::timestamp without time zone
);


##################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################


#################### Copy S3 Data over to Redshift Staging Inventory_age table  ######################################

COPY stg_inv_age
FROM 's3://prhs3/Weekly Reporting/_GET_FBA_INVENTORY_AGED_DATA_.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 



#################### Copy S3 Data over to Redshift Amz Customer Order table  ######################################

COPY stg_amz_cust_ord
FROM 's3://prhs3/Weekly Reporting/_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1;



#################### Copy S3 Data over to Redshift Staging FBA_Inventory table  ######################################

COPY stg_fba_inv
FROM 's3://prhs3/Weekly Reporting/_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 

#################### Copy S3 Data over to Redshift Fee preview table  ######################################

COPY stg_prdct_fee
FROM 's3://prhs3/Weekly Reporting/_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1
NULL AS '--';



#################### Copy S3 Data over to Redshift Staging Salesforce Account table  ######################################

COPY stg_accts
FROM 's3://prhs3/Weekly Reporting/sf_account_data.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 

#################### Copy S3 Data over to Redshift Staging Teika table  ######################################

COPY stg_teika_expt
FROM 's3://prhs3/Weekly Reporting/teika_expt.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 

#################### Copy S3 Data over to Redshift Staging Teika Orders table  ######################################

COPY stg_prch_ords
FROM 's3://prhs3/Weekly Reporting/teika_ords.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 



#################### Copy S3 Data over to Redshift Shipping Price table  ######################################

COPY shpg_px
FROM 's3://prhs3/Weekly Reporting/Shipping Pricing Table.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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' 
delimiter '\t'
IGNOREHEADER as 1; 


#################### Copy S3 Data over to Redshift Staging Case Status table  ######################################

COPY stg_case_status
FROM 's3://prhs3/case_status.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 


#################### Copy S3 Data over to Redshift Staging FBA Returns table  ######################################

COPY stg_fba_returns
FROM 's3://prhs3/Weekly Reporting/_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_.txt'
access_key_id 'AKIAISQYUM7U72YEW7IA'
secret_access_key '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'
delimiter '\t'
IGNOREHEADER as 1; 



#################### Copy S3 Data over to Redshift Staging Teika Purchase Order  ######################################

COPY stg_teika_purchase_order
FROM 's3://amify-teika-files-test/amify/amify_purchase_orders_export_02-12_2018.csv' 
iam_role 'arn:aws:iam::774126049700:role/myRedshiftRole' 
IGNOREHEADER as 1
ACCEPTINVCHARS TRIMBLANKS TRUNCATECOLUMNS CSV;



#################### Copy S3 Data over to Redshift Staging Teika Purchase Order Item  ######################################

COPY stg_teika_purchase_order_item 
FROM 's3://amify-teika-files-test/%/amify_purchase_order_items_export_%.csv', %,%
iam_role 'arn:aws:iam::774126049700:role/myRedshiftRole'
IGNOREHEADER as 1
ACCEPTINVCHARS TRIMBLANKS TRUNCATECOLUMNS CSV;


#################### Copy S3 Data over to Redshift Staging Teika Product ######################################

COPY stg_teika_product
FROM 's3://amify-teika-files-test/amify/amify_product_export_02-19_2018.csv' 
iam_role 'arn:aws:iam::774126049700:role/myRedshiftRole'
IGNOREHEADER as 1
CSV ROUNDEC;



##################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################




#################### Inserting Into Real Amz Customer Order table from staging ######################################
insert into amz_cust_ord (amz_acct, ord_id, ord_dt,ord_stus,flmt_chnl,sls_chnl,asin,qt_ordd,ccy,ttl_itm_px,ttl_itm_tx,shp_st)
Select AO.amz_acct, AO.ord_id, cast (AO.ord_dt as timestamp),AO.ord_stus,AO.flmt_chnl,AO.sls_chnl,AO.asin,AO.qt_ordd,AO.ccy,AO.ttl_itm_px,AO.ttl_itm_tx,AO.shp_st
from stg_amz_cust_ord AO 
WHERE (AO.ord_stus = 'Shipped'
OR AO.ord_stus = 'Pending')
AND sls_chnl = 'Amazon.com'
AND NOT EXISTS (select ord_id from amz_cust_ord where amz_cust_ord.ord_id = AO.ord_id and cast (ord_dt as date) = cast (AO.ord_dt as date));


#################### Update Real Amz Customer Order table from staging ######################################
update amz_cust_ord 
set ord_stus = stg_amz_cust_ord.ord_stus, updt_dttm = sysdate 
from stg_amz_cust_ord 
where amz_cust_ord.ord_id = stg_amz_cust_ord.ord_id and cast (amz_cust_ord.ord_dt as date) = cast (stg_amz_cust_ord.ord_dt as date)
and amz_cust_ord.ord_stus <> stg_amz_cust_ord.ord_stus; 




#################### Inserting Into Real Inventory Age table from staging ######################################
insert into inv_age(amz_acct,inv_dt,asin,qt_avlb,qt_0_90,qt_91_180,qt_181_270,qt_271_365,qt_365_plus,sls_past_60)
select amz_acct, to_date (inv_dt,'yyyy-mm-dd'), asin, sum(distinct qt_avlb),sum(distinct qt_0_90),sum(distinct qt_91_180),sum(distinct qt_181_270),sum(distinct qt_271_365),sum(distinct qt_365_plus),sum(distinct sls_past_60)
from stg_inv_age_2
GROUP BY inv_dt, asin, amz_acct;



#################### Inserting Into Real Inventory Age REPORT table from staging ######################################
insert into inv_age_rpt(amz_acct,inv_dt,asin,qt_avlb,qt_0_90,qt_91_180,qt_181_270,qt_271_365,qt_365_plus,sls_past_60)
select amz_acct, to_date (inv_dt,'yyyy-mm-dd'), asin, sum(distinct qt_avlb),sum(distinct qt_0_90),sum(distinct qt_91_180),sum(distinct qt_181_270),sum(distinct qt_271_365),sum(distinct qt_365_plus),sum(distinct sls_past_60)
from stg_inv_age_2
WHERE NOT EXISTS (select asin from inv_age_rpt where inv_age_rpt.asin = stg_inv_age_2.asin and cast (inv_dt as date) = cast (stg_inv_age_2.inv_dt as date))
GROUP BY inv_dt, asin, amz_acct;




#################### Inserting Into Real FBA_Inventory table from staging ######################################
insert into fba_inv (amz_acct, fba_inv_dt,asin,flbl_qt)
select amz_acct, fba_inv_dt,asin, sum(flbl_qt)
from stg_fba_inv_2
group by amz_acct, fba_inv_dt, asin;


#################### Inserting Into Real Asin-Sku table from staging ######################################
insert into asin_sku (select t1.asin, t1.sku
from stg_asin_sku t1
left join asin_sku as t2 on t1.asin = t2.asin
where t2.asin IS NULL);



###
select to_date (FI.fba_inv_dt,'yyyy-mm-dd'),FI.asin, FI.sls_px, sum (FI.flbl_qt)
from stg_fba_inv FI
where FI.sls_px IS NOT NUL
group by FI.asin, FI.fba_inv_dt; 
###


select * 
from stg_fba_inv F1
where asin = (select Distinct asin from stg_fba_inv F2 where f1.asin=f2.asin and f1.fnsku <> f2.fnsku and f2.flbl_qt > 1);



#################### Inserting Into Real prdct_fee table from staging for new ######################################
INSERT INTO prdct_fee (amz_acct, asin, prdct_wght, wght_typ, ccy, prdct_fee, lgst_sd,
                    mdn_sd, shrt_sd)
                    SELECT distinct PF.amz_acct, PF.asin, PF.prdct_wght, PF.wght_typ, PF.ccy, PF.prdct_fee,
                    PF.lgst_sd, PF.mdn_sd, PF.shrt_sd
                    FROM stg_prdct_fee PF
                    LEFT JOIN prdct_fee ON PF.asin = prdct_fee.asin
                    WHERE prdct_fee.asin IS NULL
                    AND PF.ccy != 'CAD'; 


#################### Updating Real prdct_fee table from staging ######################################
update prdct_fee
set prdct_wght = stg_prdct_fee.prdct_wght,
wght_typ = stg_prdct_fee.wght_typ,
prdct_fee = stg_prdct_fee.prdct_fee,
amz_acct = stg_prdct_fee.amz_acct,
lgst_sd = stg_prdct_fee.lgst_sd,
mdn_sd = stg_prdct_fee.mdn_sd,
shrt_sd = stg_prdct_fee.shrt_sd,
est_ref_fee = stg_prdct_fee.est_ref_fee,
updt_dttm = sysdate
from stg_prdct_fee 
where prdct_fee.asin = stg_prdct_fee.asin
AND prdct_fee.ccy = stg_prdct_fee.ccy
AND (prdct_fee.prdct_wght <> stg_prdct_fee.prdct_wght
OR prdct_fee.wght_typ <> stg_prdct_fee.wght_typ
OR prdct_fee.prdct_fee <> stg_prdct_fee.prdct_fee
OR prdct_fee.amz_acct <> stg_prdct_fee.amz_acct
OR prdct_fee.lgst_sd <> stg_prdct_fee.lgst_sd
OR prdct_fee.mdn_sd <> stg_prdct_fee.mdn_sd
OR prdct_fee.shrt_sd <> stg_prdct_fee.shrt_sd
OR prdct_fee.est_ref_fee <> stg_prdct_fee.est_ref_fee
OR prdct_fee.prdct_fee IS NULL 
OR prdct_fee.lgst_sd IS NULL
OR prdct_fee.mdn_sd IS NULL
OR prdct_fee.shrt_sd IS NULL
OR prdct_fee.prdct_wght IS NULL
OR prdct_fee.est_ref_fee IS NULL); 



#################### Inserting Real case_status table from staging ######################################

insert into case_status (transaction_item_id, fnsku, status, case_id, refund_amount, recovered_qty, submitted_date, resolved_date)
SELECT transaction_item_id, fnsku, status, case_id, refund_amount, recovered_qty, to_date(submitted_date,'mm/dd/yyyy'), 
CASE resolved_date when '' 
THEN NULL 
ELSE to_date(resolved_date,'mm/dd/yyyy')
END 
FROM stg_case_status scs 
WHERE NOT EXISTS 
(SELECT transaction_item_id 
FROM case_status cs 
WHERE cs.transaction_item_id = scs.transaction_item_id 
AND cs.fnsku=scs.fnsku 
AND cs.submitted_date = to_date (scs.submitted_date,'mm/dd/yyyy'));




#################### Updating real case_status table from staging ######################################


UPDATE case_status 
SET status = stg_case_status.status, 
case_id = stg_case_status.case_id, 
refund_amount = stg_case_status.refund_amount,
recovered_qty = stg_case_status.recovered_qty, 
resolved_date = 
CASE stg_case_status.resolved_date when '' 
THEN NULL 
ELSE to_date(stg_case_status.resolved_date,'mm/dd/yyyy')
END,
update_date = sysdate
FROM stg_case_status
WHERE case_status.transaction_item_id = stg_case_status.transaction_item_id
AND case_status.fnsku = stg_case_status.fnsku
AND case_status.submitted_date = to_date (stg_case_status.submitted_date,'mm/dd/yyyy')
AND (case_status.status <> stg_case_status.status
OR case_status.case_id <> stg_case_status.case_id
OR case_status.refund_amount <> stg_case_status.refund_amount
OR case_status.recovered_qty <> stg_case_status.recovered_qty
OR case_status.resolved_date <> CASE stg_case_status.resolved_date when '' 
THEN NULL 
ELSE to_date(stg_case_status.resolved_date,'mm/dd/yyyy')
END
OR case_status.recovered_qty IS NULL
OR case_status.refund_amount IS NULL
OR case_status.resolved_date IS NULL);


#################### Inserting Real fba_returns table from staging ######################################

INSERT INTO fba_returns (amz_account, return_date, order_id, sku, asin, fnsku, product_name, quantity, fulfillment_center_id, detailed_disposition, reason, status, license_plate_number, customer_comments) 
SELECT SFR.amz_account, cast (SFR.return_date as date), SFR.order_id, SFR.sku, SFR.asin, SFR.fnsku, SFR.product_name, SFR.quantity, SFR.fulfillment_center_id, SFR.detailed_disposition, SFR.reason, SFR.status, SFR.license_plate_number, SFR.customer_comments
FROM stg_fba_returns SFR
WHERE SFR.license_plate_number != ''
AND NOT EXISTS (select license_plate_number from fba_returns where fba_returns.license_plate_number = SFR.license_plate_number AND fba_returns.amz_account = SFR.amz_account); 


#################### Updating Real fba_returns table from staging ######################################

UPDATE fba_returns
SET detailed_disposition = stg_fba_returns.detailed_disposition,
reason = stg_fba_returns.reason,
status = stg_fba_returns.status, 
update_dt = sysdate
FROM stg_fba_returns
WHERE fba_returns.license_plate_number = stg_fba_returns.license_plate_number
AND (fba_returns.detailed_disposition <> stg_fba_returns.detailed_disposition
OR fba_returns.reason <> stg_fba_returns.reason
OR fba_returns.status <> stg_fba_returns.status); 


#################### Inserting into Real Amazon listings table from staging ######################################

INSERT INTO amazon_listings(amz_account, listing_id, sku, open_date, asin, status)
SELECT AL.amz_account, AL.listing_id, AL.sku, case AL.open_date when '' then CURRENT_DATE else cast(AL.open_date as date) end, AL.asin, AL.status
FROM stg_amazon_listings AL;

##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################



#################### Inserting Into Accounts table from Staging Salesforce table for new Accts ######################################
insert into accts (acct_id, splr_nm,acct_stus,acct_mgr,inv_mgr,free_shpg,drct_shpg,cc_acptd,quad,zip,ctgy,amz_acct)
select SA.acct_id,SA.splr_nm,SA.acct_stus,SA.acct_mgr,SA.inv_mgr,SA.free_shpg,SA.drct_shpg,SA.cc_acptd,SA.quad,SA.zip,SA.ctgy,SA.amz_acct
from stg_accts SA
LEFT JOIN accts on SA.acct_id=accts.acct_id
where accts.acct_id is null;


#################### Updating Salesforce table from Staging Salesforce table for Accts ######################################
update accts
set splr_nm = stg_accts.splr_nm, acct_stus = stg_accts.acct_stus, acct_mgr = stg_accts.acct_mgr, inv_mgr = stg_accts.inv_mgr, free_shpg = stg_accts.free_shpg, drct_shpg = stg_accts.drct_shpg, cc_acptd = stg_accts.cc_acptd, quad = stg_accts.quad, zip = stg_accts.zip, ctgy = stg_accts.ctgy, amz_acct = stg_accts.amz_acct, updt_dttm = sysdate
from stg_accts
where accts.acct_id = stg_accts.acct_id
AND (accts.splr_nm <> stg_accts.splr_nm
OR accts.acct_stus <> stg_accts.acct_stus
OR accts.acct_mgr <> stg_accts.acct_mgr
OR accts.inv_mgr <> stg_accts.inv_mgr 
OR accts.free_shpg <> stg_accts.free_shpg
OR accts.drct_shpg <> stg_accts.drct_shpg
OR accts.cc_acptd <> stg_accts.cc_acptd
OR accts.quad <> stg_accts.quad
OR accts.zip <> stg_accts.zip
OR accts.ctgy <> stg_accts.ctgy
OR accts.amz_acct <> stg_accts.amz_acct); 



##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################



#################### Inserting Into Product Supplier table from Staging Teika Products table for new Products ######################################
insert into prdct_splr (asin,splr_nm, prdct_stus)
select  stg_teika_expt.asin, split_part(stg_teika_expt.splr, ':', 1), case split_part(stg_teika_expt.splr, ':', 2) when '' then 'ACT' else split_part(stg_teika_expt.splr, ':', 2) end 
from stg_teika_expt
Left join prdct_splr on stg_teika_expt.asin = prdct_splr.asin
where prdct_splr.asin is NULL;


#################### Updating Product Splr table from Staging Teika Products table ######################################

update prdct_splr
set prdct_stus = case split_part(stg_teika_expt.splr, ':', 2) when '' then 'ACT' else split_part(stg_teika_expt.splr, ':', 2) end, splr_nm= split_part(stg_teika_expt.splr, ':', 1),  updt_dttm = sysdate 
from stg_teika_expt
where prdct_splr.asin = stg_teika_expt.asin 
and (prdct_splr.prdct_stus <> case split_part(stg_teika_expt.splr, ':', 2) when '' then 'ACT' else split_part(stg_teika_expt.splr, ':', 2) end
or prdct_splr.splr_nm <> split_part(stg_teika_expt.splr, ':', 1)); 

update prdct_splr
set splr_nm = CASE WHEN CHARINDEX(':', stg_teika_expt.splr) > 0 THEN SUBSTRING(stg_teika_expt.splr +':',0, CHARINDEX(':', stg_teika_expt.splr)) else stg_teika_expt.splr end, updt_dttm = sysdate 
from stg_teika_expt
where prdct_splr.asin = stg_teika_expt.asin 
and prdct_splr.splr_nm <> CASE WHEN CHARINDEX(':', stg_teika_expt.splr) > 0 THEN SUBSTRING(stg_teika_expt.splr +':',0, CHARINDEX(':', stg_teika_expt.splr))ELSE stg_teika_expt.splr end; 
 


#################### Inserting Into Product Costs table from Staging Teika Products table for new Products  ######################################

insert into prdct_cst (asin,prdct_cst)
select te.asin, te.prdct_cst
from stg_teika_expt TE
left join prdct_cst on TE.asin = prdct_cst.asin
where prdct_cst.asin is NULL;


#################### Updating Into Product Costs table from Staging Teika Products table ######################################

update prdct_cst
set prdct_cst = stg_teika_expt.prdct_cst, updt_dttm = sysdate 
from stg_teika_expt
where prdct_cst.asin = stg_teika_expt.asin 
and prdct_cst.prdct_cst <> stg_teika_expt.prdct_cst; 
 



#################### Inserting Into Real Teika Orders table from Staging Teika Orders ######################################
insert into prch_ords (splr,po_amt,in_tran_amt,crte_dt)
select stg_prch_ords.splr,stg_prch_ords.po_amt,stg_prch_ords.in_tran_amt, to_date (stg_prch_ords.crte_dt,'mm/dd/yyyy')
from stg_prch_ords;



#################### Inserting Into Real teika purchase order table from Staging purchase teika order ######################################

INSERT INTO teika_purchase_order (amz_account, po_id,po_created_dt, po_update_dt, po_name, status, total_ordered, total_received, total_cost, supplier_name)
SELECT '%s', SPO.po_id, CAST(SPO.po_created_dt AS DATE), CAST(SPO.po_update_dt AS DATE), SPO.po_name, SPO.status, SPO.total_ordered, SPO.total_received, SPO.total_cost, SPO.supplier_name
                   FROM stg_teika_purchase_order SPO
                   WHERE NOT EXISTS 
                   (SELECT po_id 
                   FROM teika_purchase_order 
                   WHERE teika_purchase_order.po_id = SPO.po_id AND teika_purchase_order.supplier_name = SPO.supplier_name); 


#################### Updating Into Real teika purchase order table ######################################

UPDATE teika_purchase_order
SET  po_update_dt = CAST(SPO.po_update_dt AS DATE) , total_ordered = SPO.total_ordered, total_received = SPO.total_received, total_cost = SPO.total_cost , supplier_name = SPO.supplier_name, update_dt = sysdate
                   FROM stg_teika_purchase_order SPO
                   WHERE teika_purchase_order.po_id = SPO.po_id
                   AND teika_purchase_order.supplier_name = SPO.supplier_name
                   AND( teika_purchase_order.po_update_dt <> CAST(SPO.po_update_dt AS DATE)
                   OR teika_purchase_order.total_ordered <> SPO.total_ordered
                   OR teika_purchase_order.total_received <> SPO.total_received
                   OR teika_purchase_order.total_cost <> SPO.total_cost
                   OR teika_purchase_order.po_name <> SPO.po_name);



#################### DELETE Real teika purchase order table ######################################

DELETE teika_purchase_order 
WHERE amz_account = '%s' 
AND NOT EXISTS (SELECT po_id FROM stg_teika_purchase_order SPO
WHERE teika_purchase_order.po_id = SPO.po_id AND teika_purchase_order.supplier_name = SPO.supplier_name); 


#################### Inserting Into Real teika purchase order item table from Staging purchase teika order ######################################

INSERT INTO teika_purchase_order_item (amz_account, po_id, product_name, sku, asin, mpn, total_ordered, total_received, vendor_cost, supplier_name, po_create_dt, po_update_dt) 
SELECT 'BBS', SPOI.po_id, SPOI.product_name, SPOI.sku, SPOI.asin, SPOI.mpn, SPOI.total_ordered, SPOI.total_received, SPOI.vendor_cost, SPOI.supplier_name, CAST(SPOI.po_create_dt AS DATE), CAST(SPOI.po_update_dt AS DATE)
FROM stg_teika_purchase_order_item SPOI
WHERE NOT EXISTS (SELECT po_id FROM teika_purchase_order_item WHERE teika_purchase_order_item.po_id = SPOI.po_id AND teika_purchase_order_item.sku = SPOI.sku); 


#################### Updating Into Real teika purchase order item table ######################################

UPDATE teika_purchase_order_item
SET  product_name = SPOI.product_name , asin = SPOI.asin  , mpn = SPOI.mpn  , total_ordered = SPOI.total_ordered , total_received = SPOI.total_received , vendor_cost = SPOI.vendor_cost , supplier_name = SPOI.supplier_name , po_update_dt = CAST(SPOI.po_update_dt AS DATE),  update_dt = sysdate
FROM stg_teika_purchase_order_item SPOI
WHERE teika_purchase_order_item.po_id = SPOI.po_id
AND teika_purchase_order_item.sku = SPOI.sku
AND teika_purchase_order_item.po_create_dt = CAST(SPOI.po_create_dt AS DATE)
AND (teika_purchase_order_item.product_name <> SPOI.product_name 
OR teika_purchase_order_item.asin <> SPOI.asin
OR teika_purchase_order_item.mpn <> SPOI.mpn 
OR teika_purchase_order_item.total_ordered <> SPOI.total_ordered
OR teika_purchase_order_item.total_received <> SPOI.total_received
OR teika_purchase_order_item.vendor_cost <> SPOI.vendor_cost
OR teika_purchase_order_item.supplier_name <> SPOI.supplier_name
OR teika_purchase_order_item.po_update_dt <> CAST(SPOI.po_update_dt AS DATE)); 



#################### DELETE Real teika purchase order item table ######################################

DELETE teika_purchase_order_item
WHERE amz_account = '%s'
AND NOT EXISTS (SELECT po_id FROM stg_teika_purchase_order_item SPOI
WHERE teika_purchase_order_item.po_id = SPOI.po_id AND teika_purchase_order_item.sku = SPOI.sku);



#################### Truncate Real teika purchase order item table ######################################

TRUNCATE teika_product; 


#################### Inserting Into Real teika product table from Staging teika product ######################################

INSERT INTO teika_product (amz_account, asin, sku, static_supplier_cost, additional_cost, product_update_dt, product_name, supplier_name, product_status, mpn) 
SELECT '%s', STP.asin, STP.sku, STP.static_supplier_cost, STP.additional_cost, TO_DATE(SPLIT_PART(STP.product_update_dt,' ',1),'yyyy-mm-dd'), STP.product_name,  split_part(STP.supplier_name, ':', 1), CASE split_part(STP.supplier_name, ':', 2) WHEN '' THEN 'ACT' ELSE split_part(STP.supplier_name, ':', 2) END, STP.mpn
FROM stg_teika_product STP
WHERE NOT EXISTS(SELECT asin FROM teika_product WHERE teika_product.asin = STP.asin AND teika_product.sku = STP.sku);


#################### Updating Real teika product table from Staging teika product ######################################
UPDATE teika_product
SET 
static_supplier_cost = STP.static_supplier_cost,
additional_cost = STP.additional_cost,
product_update_dt =  TO_DATE(SPLIT_PART(STP.product_update_dt,' ',1),'yyyy-mm-dd'),
product_name = STP.product_name,
supplier_name = split_part(STP.supplier_name, ':', 1),
product_status = CASE split_part(STP.supplier_name, ':', 2) WHEN '' THEN 'ACT' ELSE split_part(STP.supplier_name, ':', 2) END,
mpn = STP.mpn,
delete_flag = FALSE, 
update_dt = sysdate
FROM stg_teika_product STP
WHERE teika_product.asin = STP.asin 
AND teika_product.sku = STP.sku
AND amz_account = 'BBS' 
AND (teika_product.static_supplier_cost <> STP.static_supplier_cost
OR teika_product.additional_cost <> STP.additional_cost
OR teika_product.product_update_dt <>  TO_DATE(SPLIT_PART(STP.product_update_dt,' ',1),'yyyy-mm-dd')
OR teika_product.product_name <> STP.product_name
OR teika_product.supplier_name <> split_part(STP.supplier_name, ':', 1)
OR teika_product.product_status <> CASE split_part(STP.supplier_name, ':', 2) WHEN '' THEN 'ACT' ELSE split_part(STP.supplier_name, ':', 2) END
OR teika_product.mpn <> STP.mpn); 


UPDATE teika_product
SET delete_flag = TRUE
WHERE NOT EXISTS (
SELECT asin
FROM stg_teika_product STP
WHERE teika_product.asin = STP.asin and teika_product.sku = STP.sku)
AND amz_account = 'BBS'; 


CREATE VIEW prdct_splr AS
SELECT asin as asin, 
MAX(supplier_name) as splr_nm, 
MAX(product_status) as prdct_stus, 
CAST(NULL as varchar(10)) AS crte_dttm, 
CAST(NULL as varchar(10)) AS updt_dttm
FROM teika_product
GROUP BY asin;


CREATE VIEW prdct_cst as 
SELECT asin, 
max(static_supplier_cost) AS prdct_cst,
CAST(NULL as varchar(10)) AS crte_dttm, 
CAST(NULL as varchar(10)) AS updt_dttm  
FROM teika_product
GROUP BY asin; 



##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################



select FI.asin, staging_inventory_age.asin as inventory_age_asin
from stg_fba_inv FI
right join staging_inventory_age on FI.asin=staging_inventory_age.asin; 


##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################


#################### Delete Staging Amz Cust Order  ######################################
truncate stg_amz_cust_ord; 

#################### Delete Staging Inventory Age  ######################################
truncate stg_inv_age;

#################### Delete Staging Amz Cust Order  ######################################
truncate stg_fba_inv;




##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################

create view weekly_ords_rollover as
select

asin,

CASE WHEN date_part(w,ord_dt) = date_part(w, current_date) and datepart(y,ord_dt)= date_part(y,current_date) then ttl_itm_px else 0  END AS currentweek,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-1,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-1,current_date)) THEN ttl_itm_px ELSE 0 END AS lastweek, 

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-2,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-2,current_date)) THEN ttl_itm_px ELSE 0 END AS Week2,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-3,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-3,current_date)) THEN ttl_itm_px ELSE 0 END AS Week3,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-4,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-4,current_date)) THEN ttl_itm_px ELSE 0 END AS Week4,


CASE WHEN datepart(w,ord_dt)=datepart(w,current_date) and datepart(y,ord_dt)=datepart(y,current_date) then qt_ordd else 0  END AS currentweek_qt,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-1,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-1,current_date)) THEN qt_ordd ELSE 0 END AS lastweek_qt,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-2,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-2,current_date)) THEN qt_ordd ELSE 0 END AS Week2_qt,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-3,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-3,current_date)) THEN qt_ordd ELSE 0 END AS Week3_qt,

CASE WHEN date_part(w,ord_dt) = date_part(w, dateadd(w,-4,current_date)) AND date_part(y, ord_dt) = date_part(y,dateadd(w,-4,current_date)) THEN qt_ordd ELSE 0 END AS Week4_qt

from amz_cust_ord

where (amz_cust_ord.ord_stus= 'Shipped' OR 'Pending') 

and amz_cust_ord.sls_chnl = 'Amazon.com' ; 



#################### Create View for Inventory age   ######################################
CREATE view curr_inv_age as select  * from inv_age IA
where inv_dt = (select max(inv_dt) from inv_age IA2);



#################### Create View for FBA Inventory   ######################################
CREATE view curr_fba_inv as select  * from fba_inv FI
where fba_inv_dt = (select max(fba_inv_dt) from fba_inv FI2 where FI.asin = FI2.asin);



#################### Create View for Product Cost  ######################################
CREATE view curr_prdct_cst as select  * from prdct_cst PC
where ins_dt = (select max(ins_dt) from prdct_cst PC2 where PC.asin = PC2.asin);


#################### Create View for Purchase Orders ######################################
CREATE view curr_prch_ords as select  * from prch_ords PO
where crte_dt = (select max(crte_dt) from prch_ords PO2);
 

#################### Create View for FBA Inventory 2 ######################################
create view stg_fba_inv_2 as
select amz_acct, to_date (fba_inv_dt,'mm/dd/yyyy') as fba_inv_dt,fnsku, asin, sum(distinct flbl_qt) as flbl_qt
from stg_fba_inv
GROUP BY fba_inv_dt, fnsku, asin, amz_acct;



#################### Create View for Inventory Age 2 ######################################
create view stg_inv_age_2 as
select amz_acct, to_date (inv_dt,'yyyy-mm-dd') as inv_dt, fnsku, asin, sum(distinct qt_avlb) as qt_avlb,sum(distinct qt_0_90) as qt_0_90,sum(distinct qt_91_180) as qt_91_180 ,sum(distinct qt_181_270) as qt_181_270 ,sum(distinct qt_271_365) as qt_271_365,sum(distinct qt_365_plus) as qt_365_plus ,sum(distinct sls_past_60) as sls_past_60
from stg_inv_age
GROUP BY amz_acct, inv_dt, fnsku, asin;



#################### Create View for ASIN- Sku ######################################
create view stg_asin_sku as 
select asin, max(sku)
from stg_fba_inv
group by asin;


#################### Create View for supplier name mismatch between Teika and Salesforce ######################################
CREATE VIEW supplier_mismatch AS
SELECT amz_account, asin, sku, supplier_name, product_status
FROM teika_product
LEFT JOIN accts SF 
ON teika_product.supplier_name = SF.splr_nm
WHERE product_status = 'ACT'
AND teika_product.supplier_name != 'Filler' 
AND supplier_name != 'Test'
AND supplier_name != 'Consignment'
AND sf.splr_nm IS NULL; 



##############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################


select fba_inv.asin, fba_inv.flbl_qt, sum (inv_age.qt_avlb) as inventory_age_qt
from fba_inventory
Left Join inventory_age on fba_inventory.asin=inventory_age.asin
where fba_inventory.asin='B00K0ZN1JW'
Group by fba_inventory.asin,fba_inventory.flbl_qt, inventory_age.asin; 

select weekly_product.asin, weekly_product.product_name, fba_inventory.flbl_qt, sum (amz_cust_order.qt_ordd) as qt_ordd, sum (amz_cust_order.ttl_itm_px) as amt_sold 
from weekly_product
left join fba_inventory on weekly_product.asin=fba_inventory.asin 
left join amz_cust_order on weekly_product.asin=amz_cust_order.asin
group by weekly_product.asin,weekly_product.product_name, fba_inventory.flbl_qt; 




select f1.asin, f1.prdct_nm
from stg_fba_inv F1, stg_fba_inv F2 
where F1.asin = F2.asin 
and f1.prdct_nm <> f2.prdct_nm 




#################### FBA Inv sames asin different SKU same quantity ######################################


select amz_acct, asin, sku, flbl_qt
from stg_fba_inv
where exists 

(select asin 
from stg_fba_inv FI
where stg_fba_inv.asin = FI.asin  
AND stg_fba_inv.sku <> FI.sku
AND stg_fba_inv.flbl_qt = FI.flbl_qt
AND flbl_qt > 0);


select amz_acct, asin, sku, flbl_qt
from stg_fba_inv
where exists 

(select asin 
from stg_fba_inv FI
where stg_fba_inv.asin = FI.asin  
AND stg_fba_inv.sku <> FI.sku
AND stg_fba_inv.flbl_qt <> FI.flbl_qt);

#################################